# Jotit

Take nots on Ubuntu Touch.

[![OpenStore](https://open-store.io/badges/en_US.png)](https://open-store.io/app/jotit.christianpauly)

### How to build

1. Install clickable as described here: https://github.com/bhdouglass/clickable

2. Clone this repo:
```
git clone https://github.com/ChristianPauly/jotit
cd jotit
```

3. Build with clickable
```
clickable click-build
```
